import java.util.HashMap;
import java.util.Map;

/**
 * Command-line interface implementation of the ViewInterface
 * This is used to display the game on the command line
 */
public class CLIView implements UIViewInterface {
    // Background color constants based on most accepted solution on StackOverflow: https://stackoverflow.com/questions/5762491/how-to-print-color-in-console-using-system-out-println
    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_RED_BACKGROUND = "\u001B[41m";
    private static final String ANSI_YELLOW_BACKGROUND = "\u001B[43m";
    private static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";

    Map<Character, String> colourMap;

    public CLIView() {
        colourMap = new HashMap<>();
        // Colours do not work in windows os without using an external library
        if (!System.getProperty("os.name").toLowerCase().startsWith("windows")) {
            colourMap.put(TokenCharEnum.R.getTokenChar(), ANSI_RED_BACKGROUND);
            colourMap.put(TokenCharEnum.Y.getTokenChar(), ANSI_YELLOW_BACKGROUND);
            colourMap.put(TokenCharEnum.G.getTokenChar(), ANSI_GREEN_BACKGROUND);
        }
    }

    /**
     * Prints text to the console using system output stream.
     *
     * @param text text to display as a {@code String}
     */
    @Override
    public void displayText(String text) {
        System.out.println(text);
    }

    /**
     * Prints error message to the console using system error output stream
     *
     * @param message text to display as a {@code String}
     */
    @Override
    public void displayErrorMessage(String message) {
        System.err.println(message);
    }

    /**
     * Gets coloured representation of the token {@code char} based on colour mapping. This is basically the token char with colour
     * If no colour is defined in the colourMap for this token, an empty string is returned
     * @param tokenChar the token {@code char} to retrieve the colour
     * @return the coloured token as {@code String}
     */
    @Override
    public String getColourValue(char tokenChar) {
        String colour = colourMap.get(tokenChar);
        return (colour != null) ? (colour + tokenChar + ANSI_RESET) : "";
    }
}
