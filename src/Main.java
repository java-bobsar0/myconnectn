public class Main {
    public static void main(String[] args){
        // Using a command line display class that implements the UIViewInterface interface.
        // Owing to inheritance and polymorphism, any class that implements UIViewInterface can be instantiated here to derive another type of View - for eg: GUI
        // The type of display will be common throughout the class, so to ensure this, will pass this as an argument to the GameController constructor
        UIViewInterface ui = new CLIView();

        // Set default win condition if argument is not entered
        int winConditionNum = 4;
        if(args.length == 1){
            // Retrieve winning criteria number (N) from command line argument array (args) if present
            try {
                winConditionNum = Integer.parseInt(args[0]);
                // Following the specs requirement of 2 < N < 7.
                if (winConditionNum < 3 || winConditionNum > 6) {
                    // Throw the parent class of NumberFormatException to indicate the number has been passed in to command line as an illegal argument
                    // I prefer to throw this rather than NumberFormatException as this makes more sense here. NumberFormatException only deals with attempt to convert strings that are not numbers
                    // Due to inheritance, catching IllegalArgumentException will also catch any subclasses including the NumberFormatException that may occur due to parseInt.
                    throw new IllegalArgumentException();
                }
            } catch (IllegalArgumentException ignored) {
                ui.displayErrorMessage("Invalid command-line argument: Please enter a valid integer between 2 and 7 (excluding 2 & 7) and restart game");
                System.exit(1);
            }
        }

        String gameIntro = "Welcome to 3 Handed ConnectN\n" +
                "This is identical to Connect4 except that:\n" +
                " - The winning condition is that " + winConditionNum + " tokens of the same colour are placed in a row - vertically, horizontally or diagonally\n" +
                " - One human player competes against two computer players\n\n" +
                "Player 1 is Red, Player 2 is Yellow, Player 3 is Green\n" +
                "To play the game, type in the number of the column you want to drop you counter in\n";

        ui.displayText(gameIntro);

        // Start game
        new GameController(ui, winConditionNum);
    }
}
