import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Class responsible for controlling the actual game play
 */
class GameController {
    private final UIViewInterface ui;

    private final Board board;
    private final TokenCharEnum[] tokenCharsArr;

    private final Database<Player> playersDB;
    private final Database<Token> tokensDB;

    private int currentPlayerIndex;
    private final int winCriteriaNum;

    /**
     * Instantiates a new GameController game that takes in the winCriteria number passed in as a command line argument
     * @param ui the view implementation that renders data on screen. Interface is passed in to allow any of its implementations to be easily swapped
     * @param N the winning criteria as an {@code int}
     */
    public GameController(UIViewInterface ui, int N){
        // going for standard board size of 6x7 as the check we're performing for the command-line argument may not be suitable for some smaller board sizes
        board = new Board(N);
        this.ui = ui;
        playersDB = new Database<>();
        tokensDB = new Database<>();
        tokenCharsArr = TokenCharEnum.values();

        currentPlayerIndex = 0;
        winCriteriaNum = N;

        selectPlayers();
        playGame();
    }

    /*
     Select players based on the spec requirement of 1 Human vs 2 Computers
     If the no of players is to be extended, just add a token char to tokensCharArr and assign it to a new player here
     This can also be modified so computer players are added automatically as tokens are added (by looping through the tokens) but this is beyond the scope of the current spec requirements
     */
    private void selectPlayers() {
        if(tokenCharsArr.length == 0) {
            ui.displayErrorMessage("Internal server error: No tokens were found. Please contact the developer");
            System.exit(1);
        }
        boolean isHumanPlayerAssigned = false;

        // Assigns first token to a human and the rest to computers
        for (TokenCharEnum tokenEnum : tokenCharsArr) {
            char tokenChar = tokenEnum.getTokenChar();
            Object colour = ui.getColourValue(tokenChar);

            tokensDB.addOne(new Token(tokenChar, colour));
            if (!isHumanPlayerAssigned) {
                playersDB.addOne(new HumanPlayer("Human Player", tokenChar));
                isHumanPlayerAssigned = true;
                continue;
            }
            playersDB.addOne(new ComputerPlayer(tokenChar));
        }
    }

    /*
     Method responsible for game play.
     Game goes on until a win or a draw is met.
     This is determined by the Board class which keeps track of the state of the board and determines if N tokens of same color are placed in a row
     */
    private void playGame() {
        ui.displayText(board.toString());

        Player currentPlayer;
        boolean winnerFound = false;
        do{
            currentPlayer = getCurrentPlayer();
            ui.displayText(currentPlayer.getName() + " please enter a position:");
            try {
                // get player move. Any invalid move obtained will throw an InvalidMoveException which will be caught and displayed on screen
                int move = currentPlayer.getMove(board.getHorizontalSize());
                ui.displayText("Position " + move + " entered\n");

                // find the token object that its char value corresponds to the current player's token char
                Token tokenObj = tokensDB.getOne(currentPlayerIndex);

                if (!board.placeToken(tokenObj.getTokenChar(), tokenObj.getColour(), move)) {
                    ui.displayText("Your token was not placed as position " + move + " is filled. Please try again");
                    continue;
                }
                ui.displayText(board.toString());

                winnerFound = board.isWinnerFound(currentPlayer.getTokenChar());
                currentPlayerIndex++;
            } catch (InvalidMoveException e) {
                ui.displayErrorMessage(e.getMessage());
            }
        } while(!winnerFound && !board.isDrawFound());

        // Print a winning message if winner is found, otherwise print a draw message as that is the only other condition via which loop above can be exited
        String message = winnerFound ? currentPlayer.getName() + " Wins!!!" : "Game is a Draw!";
        ui.displayText(message);

        askToPlayAgain();
    }

    private Player getCurrentPlayer(){
        if (currentPlayerIndex >= playersDB.getTotalCount()) {
            // wrap around - go back to the first player if the last player has played
            currentPlayerIndex = 0;
        }
        return playersDB.getOne(currentPlayerIndex);
    }


    /*
    Handle play again feature if user wishes to immediately play the game again after gameplay has ended.
    This aims for a better user experience by avoiding that delay between stopping and restarting the app if user wishes to continue playing.
     */
    private void askToPlayAgain() {
        ui.displayText("\nPlay again? Enter Y for Yes or any other key to quit game.");

        // Use try-with-resources syntax to auto-close input reader after use
        try(BufferedReader input = new BufferedReader(new InputStreamReader(System.in))) {
            String line = input.readLine();

            if (line.equals("Y") || line.equals("y")) {
                new GameController(ui, winCriteriaNum);
            } else {
                ui.displayText("Thank you for playing! Goodbye.");
            }
        } catch (IOException e) {
            ui.displayErrorMessage("An error occurred while trying to read your input. Please restart the game");
            // Exit the app gracefully
            System.exit(1);
        }
    }

}