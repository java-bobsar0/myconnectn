/**
 * Enum representation for token characters used in the game
 * This helps detect any errors that may arise from hardcoding, adding or removing token chars at compile-time
 * Any token used is added here and it's easy to detect when duplicate tokens are used
 */
public enum TokenCharEnum {
    R('r'),
    Y('y'),
    G('g');

    private final char tokenChar;

    TokenCharEnum(char tokenChar) {
        this.tokenChar = tokenChar;
    }

    /**
     * Gets the char value of the token enum
     *
     * @return the enum token value as {@code char}
     */
    public char getTokenChar() {
        return tokenChar;
    }
}
