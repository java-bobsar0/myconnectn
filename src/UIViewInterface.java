/**
 * Interface responsible for displaying data on the screen
 * Any class wishing to render data on the screen must implement all methods defined by this class
 */
public interface UIViewInterface {
    /**
     * Renders text to the screen
     *
     * @param text text to display as a {@code String}
     */
    void displayText(String text);

    /**
     * Renders error message to the screen
     *
     * @param msg message to display as a {@code String}
     */
    void displayErrorMessage(String msg);

    /**
     * Gets colour value of a character which could be used to render the character
     *
     * @param tokenChar the character to determine the colour
     * @return the colour value of this character as an {@code Object}.
     * Different view implementations may return the colour in different types hence {@code Object} type is returned here as every other class is a subclass of {@code Object}
     */
    Object getColourValue(char tokenChar);
}
