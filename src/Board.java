import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Board class used to create the board object the game is played on and implement its associated behaviours
 */
public class Board {
    private final int horizontalSize;
    private final int verticalSize;
    private final int winCriteriaNum;

    // Map that defines the board. The key is the horizontal/column number
    private Map<Integer, List<Cell>> board;

    /**
     * Creates a new Board object with pre-defined standard size of 6x7
     */
    public Board(int N) {
        this.verticalSize = 6;
        this.horizontalSize = 7;
        winCriteriaNum = N;

        populateBoardCells();
    }

    /**
     * Creates a new Board object of any size based on the passed-in vertical and horizontal sizes arguments
     *
     * @param verticalSize   the vertical or y-axis board size
     * @param horizontalSize the horizontal or x-axis board size
     */
    public Board(int verticalSize, int horizontalSize, int N) {
        this.verticalSize = verticalSize;
        this.horizontalSize = horizontalSize;
        winCriteriaNum = N;

        populateBoardCells();
    }

    /*
    Initializes board cells using horizontal axis of the board as Map's key and the vertical size as the number of cells in the Map's List value
     Key of map corresponds to a board column
     */
    private void populateBoardCells() {
        board = new HashMap<>();

        for (int i = 1; i <= horizontalSize; i++) {
            List<Cell> newCells = new ArrayList<>();

            for (int j = 1; j <= verticalSize; j++) {
                Cell newCell = new Cell();
                newCells.add(newCell);
            }
            board.put(i, newCells);
        }
    }

    /**
     * Retrieves the horizontal size of the board
     *
     * @return horizontal size as an {@code int}
     */
    public int getHorizontalSize() {
        return horizontalSize;
    }

    /**
     * Overrides the toString() method to return the {@code String} value of {@code Board} object.
     */
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();

        for (int i = verticalSize - 1; i >= 0; i--) {
            for (int key = 1; key <= board.size(); key++) {
                List<Cell> cellList = board.get(key);
                Cell cell = cellList.get(i);
                // calls cell.toString() implicitly
                s.append(cell);
            }
            s.append("|").append("\n");
        }
        s.append("  ");
        // appends numbers on the horizontal axis corresponding to each board column
        for (int i = 1; i <= horizontalSize; i++) {
            s.append(i).append("   ");
        }
        s.append("\n");

        return String.valueOf(s);
    }

    /**
     * Places tokenChar on the defined position on the board. Position maps to the key/column in board Map
     *
     * @param tokenChar the tokenChar to be placed.
     * @param colour    the colour representation of the token to be placed
     * @param column    the position/column on the board to place the tokenChar in.
     * @return true or false representing whether the tokenChar is placed successfully or not
     */
    public boolean placeToken(char tokenChar, String colour, int column) {
        List<Cell> cells = board.get(column);

        for (Cell cell : cells) {
            if (!cell.isOccupied()) {
                cell.setTokenChar(tokenChar);
                cell.setColourValue(colour);
                cell.setOccupied(true);
                return true;
            }
        }
        return false;
    }

    /**
     * Checks horizontal, vertical and diagonal directions to determine if winner is found
     * Winner is determined when 4 tokens of same color are connected in a row in either horizontal, vertical or diagonal direction
     *
     * @param token the token to be checked
     * @return true or false representing if a winner is found or not
     */
    public boolean isWinnerFound(char token) {
        return checkHorizontalWin(token) ||
                checkVerticalWin(token) ||
                checkRightDiagonalWin(token) ||
                checkLeftDiagonalWin(token);
    }

    /**
     * Checks if the game is a draw by traversing through the board to determine if all positions have been filled
     *
     * @return true or false representing if the game is a draw or not
     */
    public boolean isDrawFound() {
        for (List<Cell> cells : board.values()) {
            for (Cell cell : cells) {
                if (!cell.isOccupied()) {
                    return false;
                }
            }
        }
        return true;
    }

    /*
     Checks if the token is connected 4 times in a row in the horizontal direction
     */
    private boolean checkHorizontalWin(char token) {
        int count = 0;
        // board map is stored with horizontal/column values as keys
        for (int i = 0; i < verticalSize; i++) {
            for (int column = 1; column <= horizontalSize; column++) {

                List<Cell> cellList = board.get(column);
                Cell cell = cellList.get(i);

                if (cell.isOccupied() && cell.getTokenChar() == token) {
                    count++;
                    if (hasWon(count)) return true;
                } else {
                    // reset count if no similar token in immediate next position was found
                    count = 0;
                }
            }
            // reset count if we reach a board edge
            count = 0;
        }
        return false;
    }

    /*
     Checks if the token is connected 4 times in a row in the vertical direction
     */
    private boolean checkVerticalWin(char token) {
        int count = 0;

        for (int column = 1; column <= horizontalSize; column++) {
            List<Cell> cellList = board.get(column);

            for (Cell cell : cellList) {
                if (cell.isOccupied() && cell.getTokenChar() == token) {
                    count++;
                    if (hasWon(count)) return true;
                } else {
                    // reset count if no similar token in immediate next position was found
                    count = 0;
                }
            }
            // reset count if we reach a board edge
            count = 0;
        }
        return false;
    }

    private boolean checkRightDiagonalWin(char token) {
        return checkDiagonalWin(token, Diagonal.RIGHT);
    }

    private boolean checkLeftDiagonalWin(char token) {
        return checkDiagonalWin(token, Diagonal.LEFT);
    }

    /*
    Check if a win occurred in the diagonal direction based on passed in direction
    "direction" is an enum value as this is better than hard-coding strings - less error prone
     */
    private boolean checkDiagonalWin(char token, Diagonal direction) {
        int count = 0;
        for (int i = 0; i < verticalSize; i++) {
            // create a separate variable to query list so we dont overwrite i as the variable is going to be manipulated within inner for loop
            int tempIndexToCheck = i;

            for (int column = 1; column <= horizontalSize; column++) {
                List<Cell> cellList = board.get(column);
                try {
                    Cell cell = cellList.get(tempIndexToCheck);

                    // check if the token in the current cell is the same in the cell in the immediate upper right or left position depending on direction
                    // we increment or decrement tempIndexToCheck if an immediate similar token and perform check again
                    if (cell.isOccupied() && cell.getTokenChar() == token) {
                        count++;
                        if (hasWon(count)) return true;

                        if (direction == Diagonal.RIGHT) {
                            tempIndexToCheck++;
                        } else if (direction == Diagonal.LEFT) {
                            tempIndexToCheck--;
                        }
                        continue;
                    }
                    count = 0;
                    // reset tempIndex if no immediate similar token is found;
                    tempIndexToCheck = i;
                    // if tempIndex is outside the range of cellList, catch the exception thrown below and reset count and tempIndex
                } catch (IndexOutOfBoundsException ignored) {
                    count = 0;
                    tempIndexToCheck = i;
                }
            }
            // reset count if we reach a board edge
            count = 0;
        }
        return false;
    }

    // User wins if token is connected at least "winCriteriaNum" rows in succession
    private boolean hasWon(int count) {
        return count >= winCriteriaNum;
    }

    // Enum representing right and left diagonal direction values.
    private enum Diagonal {RIGHT, LEFT}
}