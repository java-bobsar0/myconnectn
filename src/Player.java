/**
 * Abstract Class representing the users of the game
 */
public abstract class Player {
    private final String name;
    private final char tokenChar;

    /**
     * Creates a new Player object for the game
     * @param name  name of the player
     * @param token the {@code char} counter that belongs to this player
     */
    public Player(String name, char token){
        this.name = name;
        this.tokenChar = token;
    }

    /**
     * Get player name concatenated with the token character
     * @return the Player's name as a {@code String}
     */
    public String getName() {
        return name+"[" + tokenChar + "]";
    }

    /**
     * Gets token character held by the player
     * @return the token character as a {@code char}
     */
    public char getTokenChar() {
        return tokenChar;
    }

    /**
     * Retrieves the integer value of user input.
     * @param limit max input value required
     * @return user input as an {@code int}
     */
    public abstract int getMove(int limit);
}

