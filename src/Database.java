import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * A Database Generic class that stores objects of type {@code T}. This acts a layer of abstraction to the underlying class type used
 * A Generic class is used here so that the Database class can be written once and used for any Object type {@code T} - without needing to create separate classes for them
 * @param <T> the class Type to store in this database object
 */
public class Database<T> {

    // declare list of type T
    private final List<T> items;

    public Database(){
        items = new ArrayList<>();
    }

    public void addOne(T item){
        items.add(item);
    }

    public void addAll(Collection<T> items){
        this.items.addAll(items);
    }

    public T getOne(int index){
        return items.get(index);
    }

    public int getTotalCount() {
        return items.size();
    }
}
