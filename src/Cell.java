/**
 * Class that represents a specific block of a board where a token is placed.
 * A board is a collection of {@code Cell} objects
 */
public class Cell {
    private char tokenChar;
    private boolean isOccupied;
    private String colourValue;

    public Cell() {
        isOccupied = false;
        colourValue = "";
    }

    /**
     * Gets the status of the cell - whether it holds a token or not
     * @return true/false corresponding to whether the cell is occupied
     */
    public boolean isOccupied() {
        return isOccupied;
    }

    /**
     * Changes the occupied status of the cell
     * @param occupied the new value to change the occupied status to
     */
    public void setOccupied(boolean occupied) {
        isOccupied = occupied;
    }

    /**
     * Gets the token char contained in the cell
     * @return the token as a {@code char}
     */
    public char getTokenChar() {
        return tokenChar;
    }

    /**
     * Sets the token value held in the cell
     * @param tokenChar the token char to set the token to
     */
    public void setTokenChar(char tokenChar) {
        this.tokenChar = tokenChar;
    }

    /**
     * Sets the colour value of the cell.
     * The cell will have this colour when printed unless the colourValue is an empty string
     * @param colourValue the colour of the cell
     */
    public void setColourValue(String colourValue) {
        this.colourValue = colourValue;
    }

    /**
     * Overrides the toString method to provide a {@code String} representation of this cell
     * This is useful in printing the cell
     * @return the {@code} String representation of the cell
     */
    @Override
    public String toString() {
        if(isOccupied) {
            String cell = !colourValue.equals("") ? colourValue : String.valueOf(tokenChar);
            return "| " + cell + " ";
        }
        return "|   ";
    }
}
