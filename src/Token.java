/**
 * Class representing the token object that is used to play the game
 */
public class Token {
    private final char tokenChar;
    private final Object colourRepresentation;

    /**
     * Instantiates the token object to be used by the players of the game
     *
     * @param token  the representation of the token as a {@code char}
     * @param colour the coloured representation of the token as an {@code Object}.
     *               An object is used here as different views might render colours using different Class types. Every class is a subclass of an object so will be allowed here
     */
    public Token(char token, Object colour) {
        this.tokenChar = token;
        colourRepresentation = colour;
    }

    public char getTokenChar() {
        return tokenChar;
    }

    public String getColour() {
        return colourRepresentation != null ? colourRepresentation.toString() : String.valueOf(tokenChar);
    }

    /**
     * Overrides toString method to give the {@code Token} object {@code String} representation as either the tokenChar or its coloured representation if present
     *
     * @return the {@code Token} object as a {@code String}
     */
    @Override
    public String toString() {
        return colourRepresentation != null ? colourRepresentation.toString() : String.valueOf(tokenChar);
    }
}
